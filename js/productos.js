
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getDatabase, onValue, ref, set, child, get, update, remove } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyBZixrAHKUfPwjekx4Cp0MWkNsQAVK5mXA",
    authDomain: "sitiofinal-3c973.firebaseapp.com",
    projectId: "sitiofinal-3c973",
    storageBucket: "sitiofinal-3c973.appspot.com",
    messagingSenderId: "920402060023",
    appId: "1:920402060023:web:ec6296358e9e3fd4e269f2",
    databaseURL: "https://sitiofinal-3c973-default-rtdb.firebaseio.com/"
};



// Initialize Firebase
const app = initializeApp(firebaseConfig);

var cursosp = document.getElementById('a');


function mostrar() {
    const db = getDatabase();
    const dbRef = ref(db, 'productos');
    onValue(dbRef, (snapshot) => {
        cursosp.innerHTML = "";
        snapshot.forEach((childSnapshot) => {

            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();
            if (childData.Status == "Disponible") {
                cursosp.innerHTML = cursosp.innerHTML + "<div id='cur'>" +
                    "<center>" +
                    "<img src='" + childData.url + "' alt=''>" +
                    "<h3>" + "" + childData.nombre + "</h3>" +
                    "<h4>Características:</h4>" +
                    "<li>" + childData.descripcion + "</li><br>" +
                    "<a href=''>$" + childData.precio + "MXN</a>" + "<br>" +
                    "<button id='btn2' >Comprar</button>" +
                    "<br>" +
                    "</center></div>";
            }
        });
        {
            onlyOnce: true
        }
    });
}
window.onload(mostrar());