import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getAuth, signInWithEmailAndPassword, createUserWithEmailAndPassword, signOut } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";
import { getFirestore, doc, getDoc, getDocs, collection }
  from "https://www.gstatic.com/firebasejs/9.13.0/firebase-firestore.js";
import { getDatabase, onValue, ref, set, child, get, update, remove }
  from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";

const firebaseConfig = {
  apiKey: "AIzaSyBZixrAHKUfPwjekx4Cp0MWkNsQAVK5mXA",
  authDomain: "sitiofinal-3c973.firebaseapp.com",
  projectId: "sitiofinal-3c973",
  storageBucket: "sitiofinal-3c973.appspot.com",
  messagingSenderId: "920402060023",
  appId: "1:920402060023:web:ec6296358e9e3fd4e269f2",
  databaseURL: "https://sitiofinal-3c973-default-rtdb.firebaseio.com/"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const db = getDatabase();

    loginbtn.addEventListener('click', function(){
    const loginEmail = document.getElementById("loginemail").value;
    const loginPassword = document.getElementById("loginpassword").value;

   signInWithEmailAndPassword(auth, loginEmail, loginPassword)
  .then((userCredential) => {
    const user = userCredential.user;
    document.getElementById("logindiv").style.display = "none";
    location.href = "administrador.html";
    
  })
  .catch((error) => {
    const errorCode = error.code;
    const errorMessage = error.message;
    document.getElementById("logindiv").style.display="none";
    alert("Correo o contraseña incorrecto: " + errorMessage);
    location.href = "error.html";

  });
});



document.getElementById("btnlimpiar").addEventListener('click', function () {
  const loginEmail = document.getElementById("loginemail").value="";
  const loginPassword = document.getElementById("loginpassword").value="";
});


